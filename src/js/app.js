let value = "favourit";
const log = console.log;

// Selecting elements from the DOM
const searchButton = document.querySelector('.search-btn');
const searchInput = document.querySelector('#searchBar');
const moviesSearchable = document.querySelector('.movie-results-wrapper');
const movieDetail = document.querySelector('.movie-detail-wrapper');
const movieInfo = document.querySelector('.movie-info');
const movieImage = document.querySelector('.movie-image');
const favBtn = document.querySelector('.favorite-btn');



function handleGeneralError(error) {
    log('Error: ', error.message);
    alert(error.message || 'Server is not working fine');
}

// this is search button for movies
searchButton.onclick = function (event) {
    event.preventDefault();
    value = searchInput.value
    movieInfo.innerHTML = "";
    movieImage.innerHTML = "";
    if (movieDetail.childElementCount > 1) {
        movieDetail.removeChild(movieDetail.childNodes[1]);  
    }
   if (value) {
    searchMovie(value);
   }
   
}

// getting the list of particular type of movie type
function renderSearchMovies(data) {
    moviesSearchable.innerHTML = '';
    const moviesBlock = generateMoviesBlock(data);
    const resultHeader = document.createElement('h3');
    resultHeader.style.paddingTop = "15px";
    let content;
    if (data.length == 0) {
        alert('Please add movies in list')
        movieInfo.innerHTML = "";
        movieImage.innerHTML = "";
        if (movieDetail.childElementCount > 1) {
            movieDetail.removeChild(movieDetail.childNodes[1]);  
        }
        return ;
    }
    else {
        if (value === 'favourit'){
            content = `Favourite`;    
        }
        else
        content = `${value}`;
        resultHeader.innerHTML = content;
    }
    moviesSearchable.appendChild(resultHeader);
    moviesSearchable.appendChild(moviesBlock);
}

function generateMoviesBlock(data) {

    const movies = data;
    console.log(movies);
    const section = document.createElement('section');
    section.setAttribute('class', 'section');
    section.style.display = "flex";
    section.style.overflow = "scroll"

    for (let i = 0; i < movies.length; i++) {
            const title = movies[i].title
            const imageUrl = movies[i].posterurl;
            const id = movies[i].id;
            const imageContainer = createImageContainer(imageUrl, id,title);
            section.appendChild(imageContainer);
    }

    const movieSectionAndContent = createMovieContainer(section);
    return movieSectionAndContent;
}



function createMovieContainer(section) {
    const movieElement = document.createElement('div');
    movieElement.setAttribute('class', 'movie');
    movieElement.insertBefore(section, movieElement.firstChild);
    
    return movieElement;
}

function createImageContainer(imageUrl, id,movieTitle) {
    const tempDiv = document.createElement('div');
    tempDiv.style.paddingTop = "30px";
    tempDiv.setAttribute('class', 'imageContainer');
    tempDiv.setAttribute('data-id', id);
    tempDiv.setAttribute('movie-type',value);

    const movieElement = `
        <div style = "margin-left : 15px;">
        <img src="${imageUrl}" alt="image" data-movie-id="${id}" style = "width: 200px;height: 200px;">
        <p style = "text-align : center;padding-top: 15px;font-weight : bold;">${movieTitle}</p>
    
        </div>
    `;
    tempDiv.innerHTML = movieElement;

    return tempDiv;
}




function renderMovie(data) {
    console.log(data);
    let parent = document.querySelector('.movie-detail-wrapper');
    if (parent.childElementCount < 2) {
        let details = document.createElement('h2');
        details.innerHTML = "Movie Details";
        details.style.textAlign  = "left";
        details.style.marginBottom = "30px"
        parent.insertBefore(details,parent.firstElementChild); 
    }
    const tempDiv = document.createElement('div');
    const imgTemplate = 
        `<img class = "img-responsive" src = "${data.posterurl}" alt = "No image available" style ="width: 100%;height: 100%;">`;
    tempDiv.innerHTML = imgTemplate;
    if (movieImage.hasChildNodes()) {
        movieImage.innerHTML = "";
    }
    movieImage.appendChild(tempDiv);
    const detailTemplate = generateDetailTemplate(data);

    if (movieInfo.hasChildNodes()) {
        movieInfo.innerHTML = "";
    }
    movieInfo.appendChild(detailTemplate);
}

function createParagraph(key,value) {
    const newVal = `<strong>${key}</strong> - ${value}`;
    const p = document.createElement('p');
    p.innerHTML = newVal; 
    return p;
}

function generateDetailTemplate(data) {
    
    const element = document.createElement('div');
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            val = data[key];
            element.appendChild(createParagraph(key[0].toUpperCase() + key.substr(1),val));
        }
    }
    const btnDiv = document.createElement('div');
    btnDiv.innerHTML = `<button class = "btn btn-primary" onclick = "addToFavourite(${data.id})" style = "margin-bottom : 20px;">Add Favourite</button>`;
    const remFavBtnDiv = document.createElement('div');
    remFavBtnDiv.innerHTML = `<button class = "btn btn-primary" onclick = "deleteFromFavourite(${data.id})">Delete Favourite</button>`;
    element.appendChild(btnDiv);
    element.appendChild(remFavBtnDiv);
    return element;
}

moviesSearchable.onclick = function(event) {
    const movieId  = event.target.getAttribute('data-movie-id')
    const movieType = value;
    getMovieFromId(movieId,movieType);
}



favBtn.onclick = function (event) {
    event.preventDefault();
    value = 'favourit';
    searchMovie(value);
   
}


const addToFavourite = (id) => {
    addFavouriteMovie(id,value);
}

const deleteFromFavourite = (id) => {
    deleteFavouriteMovie(id,'favourit');
}