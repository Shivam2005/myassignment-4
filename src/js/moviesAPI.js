
const defaultUrl = 'http://localhost:3000';

// here we are generating the urls
function generateUrl(value) {
    return `${defaultUrl}/${value}`;
}

// fetching particlular api
function requestMovies(url, onComplete, onError) {
    fetch(url)
        .then((res) => res.json())
        .then(onComplete)
        .catch(onError);
}

function searchMovie(value) {
    const url = generateUrl( value);
    requestMovies(url, renderSearchMovies, handleGeneralError);
}


function getMovieFromId(movieId,movieType) {
    let url = generateUrl(movieType)
    url = `${url}/${movieId}`;
    requestMovies(url,renderMovie,handleGeneralError);
}


// movies are added in the favourite list here
function getAndAdd(url) {
    fetch(url)
        .then((res) => res.json())
        .then((data) => {
            console.log('Hi');
            const {id,...newObj} = data;
            console.log(newObj);

            const resp = fetch("http://localhost:3000/favourit", {
                    method: 'POST', 
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(newObj) // body data type must match "Content-Type" header
            });
            resp.then(data => {
                console.log(data);
            })
        })
        .catch((error) => {
            alert(error.message || 'Server is not working fine')
        });
}
function addFavouriteMovie(id,type) {
    let url = generateUrl(type);
    url = `${url}/${id}`;
    getAndAdd(url);
}



//here we are deleting the movie using its id
function deleteMovie(url) {
    // searching the movie using its id
    fetch(url)
        .then((res) => res.json())
        .then((data) => {
            console.log('inside delete');
            const resp = fetch(`http://localhost:3000/favourit/${data.id}`, {
                    method: 'DELETE', 
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data) 
            });
            resp.then(data => {
                console.log(data);
            })
        })
        .catch((error) => {
            alert(error.message || 'server is not working fine')
        });
}

function deleteFavouriteMovie(id,type) {
    let url = generateUrl(type);
    url = `${url}/${id}`;
    deleteMovie(url);
}
